#!/bin/bash

# mainly for serving site via (GitLab) build pipeline

rm -R public/*
echo "-- public/ content removed --"
echo "-- generating new static files --"
hugo
echo "-- DONE --"
