# AW Hugo Starter

My customized Hugo-based starter boilerplace, built on [jimfrenette/hugo-starter](https://github.com/jimfrenette/hugo-starter) theme.

---

## TODO & features

- [ ] theme.toml proper info
- [ ] https://microformats.io
  - [x] blog post detail (http://microformats.org/wiki/h-entry)
  - [x] about page / homepage (maybe in footer @ copyright or hidden visually?)
  - [ ] events (http://microformats.org/wiki/h-event)
  - [ ] review (http://microformats.org/wiki/h-review)
  - [ ] recipe (http://microformats.org/wiki/h-recipe)
  - [x] resume (http://microformats.org/wiki/h-resume)
    - [ ] o:g for profile all right?
    - [ ] custom page-header partial for resume?
    - [ ] print version
- [ ] ARIA accessibility + tweaks
  - [ ] ARIA (AA = OK)
  - [ ] contrasts
    - 4.5:1 - normal text
    - 3:1 - large text, icons, form elements (input borders, checkboxes..)
  - [ ] prefers-reduced-motion (remove %tfx)
  - [ ] prefers-dark-theme (or how's it called..)
- [ ] image lazy-loading
- [ ] Netlify CMS integration

- [ ] validation
  - HTML
    - [ ] https://validator.w3.org/nu/
  - ARIA
    - [ ] Simple Accessibility Checklist: https://a11yproject.com/checklist/
    - [ ] Firefox's Accessibility Inspector: https://developer.mozilla.org/en-US/docs/Tools/Accessibility_inspector
    - [ ] WAVE: https://wave.webaim.org/
    - [ ] ARIA widget checklist: https://www.levelaccess.com/aria-widget-checklist-screen-reader-testing/

---

## Installation & Setup

==To use for a new project==, simply copy-paste this repo inside a new project. I've tried with git submodules earlier, but it's creating more problems than useful features.

### Posts

- Site authors: add `.yaml` file for each author in `/data/authors/` (e.g. `/data/authors/Martin_Allien.yaml`) and fill in the fields, based on the example - these will be used in posts and other content where `author: martin` is present in file's frontmatter

### Resume

- Fill in `/data/resume.yaml`, check that `/content/resume/index.md` exists and has `layout: resume` present - this will generate Resume page based on the data entered

---

## Development

Start Hugo dev server

    yarn run hugo

### Webpack Dev UI

Install node modules

    yarn install

Unminified development build with sourcemaps

    yarn run dev

Build for production. CSS and JavaScript files will be output into the starter themes `/dist` folder

    yarn run build


### Production

To run in production (e.g. to have Google Analytics show up), run `HUGO_ENV=production` before your build command. For example:

```
HUGO_ENV=production hugo
```

---

## Upgrading

Fetch latest updates (webpack/*, package.json) from https://github.com/jimfrenette/hugo-starter/.


## Notes

Source: [Hugo Static Site Generator Blank Starter Theme](https://jimfrenette.com/2019/02/hugo-static-site-generator-blank-starter-theme/)
