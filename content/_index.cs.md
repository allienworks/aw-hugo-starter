---
title: "Úvod"
summary: "This is the homepage (for open:graph etc.)!"
slug: ""
class: ""
menu:
  main:
    weight: 1
---

Vítejte na úvodní stránce.

> The flier should feel like a warm handshake there is too much white space just do what you think. I trust you, make it original, yet I really think this could go viral, nor make it pop.

